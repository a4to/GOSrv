package middleware

import (
  "encoding/json"
  "io/ioutil"
  "github.com/gin-gonic/gin"
  "net/http"
  "os"
)

var dataFile = "storage/data.json"

func ensureFileExists() {
  if _, err := os.Stat(dataFile); os.IsNotExist(err) {
    ioutil.WriteFile(dataFile, []byte("[]"), 0644)
  }
}

func savePostRequest(c *gin.Context) {
  ensureFileExists()

  var requestData map[string]interface{}
  if err := c.BindJSON(&requestData); err != nil {
    c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
    c.Abort()
    return
  }

  file, err := ioutil.ReadFile(dataFile)
  if err != nil {
    c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to read data file"})
    c.Abort()
    return
  }

  var data []map[string]interface{}
  json.Unmarshal(file, &data)
  data = append(data, requestData)

  newData, err := json.MarshalIndent(data, "", "  ")
  if err != nil {
    c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to marshal data"})
    c.Abort()
    return
  }

  ioutil.WriteFile(dataFile, newData, 0644)
  c.Next()
}

func JSONStorageMiddleware() gin.HandlerFunc {
  return savePostRequest
}
