package middleware

import (
  "github.com/gin-gonic/gin"
  "github.com/dgrijalva/jwt-go"
  "net/http"
  "time"
)

var jwtSecret = []byte("secret_key")

func GenerateToken(userID string) (string, error) {
  token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
    "user_id": userID,
    "exp":     time.Now().Add(time.Hour * 72).Unix(),
  })

  tokenString, err := token.SignedString(jwtSecret)
  if err != nil {
    return "", err
  }

  return tokenString, nil
}

func AuthMiddleware() gin.HandlerFunc {
  return func(c *gin.Context) {
    tokenString := c.GetHeader("Authorization")
    if tokenString == "" {
      c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header required"})
      c.Abort()
      return
    }

    token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
      return jwtSecret, nil
    })

    if err != nil || !token.Valid {
      c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
      c.Abort()
      return
    }

    claims, ok := token.Claims.(jwt.MapClaims)
    if !ok || !token.Valid {
      c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token claims"})
      c.Abort()
      return
    }

    userID := claims["user_id"].(string)
    c.Set("user_id", userID)
    c.Next()
  }
}
