package routes

import (
  "github.com/gin-gonic/gin"
  "gitlab.com/a4to/GOSrv/server"
  "gitlab.com/a4to/GOSrv/handlers"
)

func RegisterRoutes(s *server.Server) {
  s.AddRoute("GET", "/", homeHandler)
  s.AddRoute("POST", "/data", middleware.JSONStorageMiddleware(), postDataHandler)
}

func homeHandler(c *gin.Context) {
  s.RenderTemplate(c, "index.html", gin.H{"title": "Home Page"})
}

func postDataHandler(c *gin.Context) {
  c.JSON(200, gin.H{"status": "data received"})
}
