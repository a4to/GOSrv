package main

import (
  "gitlab.com/a4to/GOSrv/routes"
  "gitlab.com/a4to/GOSrv/server"
  "gitlab.com/a4to/GOSrv/handlers"
)

func main() {
  s := server.NewServer()
  s.AddMiddleware(middleware.AuthMiddleware())
  routes.RegisterRoutes(s)
  s.Run(":7474")
}

