package server

import (
  "html/template"
  "github.com/gin-gonic/gin"
  "net/http"
)

type Server struct {
  router *gin.Engine
}

func NewServer() *Server {
  router := gin.Default()
  return &Server{router: router}
}

func (s *Server) AddRoute(method string, path string, handler gin.HandlerFunc) {
  switch method {
  case "GET":
    s.router.GET(path, handler)
  case "POST":
    s.router.POST(path, handler)
  }
}

func (s *Server) AddRouteWithMiddleware(method string, path string, middleware gin.HandlerFunc, handler gin.HandlerFunc) {
  switch method {
  case "GET":
    s.router.GET(path, middleware, handler)
  case "POST":
    s.router.POST(path, middleware, handler)
  }
}

func (s *Server) AddMiddleware(middleware gin.HandlerFunc) {
  s.router.Use(middleware)
}

func (s *Server) RenderTemplate(c *gin.Context, name string, data interface{}) {
  tmpl, err := template.ParseFiles("templates/" + name)
  if err != nil {
    c.String(http.StatusInternalServerError, "Template rendering error: %s", err)
    return
  }
  tmpl.Execute(c.Writer, data)
}

func (s *Server) Run(port string) {
  s.router.Run(port)
}
